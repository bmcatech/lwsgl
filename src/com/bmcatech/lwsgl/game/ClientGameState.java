package com.bmcatech.lwsgl.game;

import java.awt.*;

/**
 * Created by cahenk on 5/17/15.
 */
public class ClientGameState extends GameState{

    public ClientGameState(int id, StateBasedGame sbg){
        super(id, sbg);
    }

    public void init(){

    }

    public void render(Graphics g){

    }

    public void update(){

    }

}
